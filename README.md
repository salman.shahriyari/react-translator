# React sign Language Translator

Translator is an application aimed to translate words to sign language(character by character) for those with hearing inabilities. It uses a dictinoary of pictures for each letter and shows the picture of it. It also saves your translation history in the api.


## Getting Started
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.


### `npm run build`

Builds the app for production to the `build` folder.\

### Dependencies

* First need to install npm: npm install

### Executing program

* Download the program code and run it in your local machine. 
* Then you are free to make your required changes.

## Usage

* At first, your username is asked and then you can see your profile.

* From there, it is possible to navigate to translation page to make a new translation. 

* after click the submit button for translation, it is saved in your translation history.


## Contributing
It is a demo app and you are welcome to give your opinions to make the app features and its visualisation better


## Authors

* Salman Shahriyari  
* [@salman3sh]

## Version History

* 0.1
    * Initial Release


## Acknowledgments and inspiration

* Dewald Els
* Noroff.no








