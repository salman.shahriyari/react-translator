import { useState } from "react";
import { TranslationAdd } from "../api/translation";
import TranslationsForm from "../components/Translations/TranslationsForm";
import TranslationsSummary from "../components/Translations/TranslationsSummary";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";

const Translations = () => {
  const [text, setText] = useState(null);
  const { user, setUser } = useUser();

  const handleTranslationClicked = async (notes) => {
    let translation = notes
      .toLowerCase()
      .replace(/[^a-zA-Z]/g, "")
      .trim();

    setText(translation);
    const [error, updatedUser] = await TranslationAdd(user, translation);

    if (error !== null) {
      return;
    }

    storageSave(STORAGE_KEY_USER, updatedUser);
    setUser(updatedUser);

    console.log(error);
    console.log(updatedUser);
  };

  return (
    <>
      <h1>Translations</h1>

      <section id="translation-notes">
        <TranslationsForm onTranslation={handleTranslationClicked} />
      </section>
      {text && <TranslationsSummary notes={text} />}
    </>
  );
};

export default withAuth(Translations);
