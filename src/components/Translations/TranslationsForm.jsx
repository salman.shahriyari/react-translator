import { useForm } from "react-hook-form";

const TranslationsForm = ({ onTranslation }) => {
  const { register, handleSubmit } = useForm();

  const onSubmit = ({ translationNotes }) => {
    onTranslation(translationNotes);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <fieldset>
        <label htmlFor="translation-notes">Translation: </label>
        <input
          type="text"
          {...register("translationNotes")}
          placeholder="Hello"
        />
      </fieldset>
      <button type="submit">Translate</button>
    </form>
  );
};

export default TranslationsForm;
