const printImages = (str) => {
  return <img src={"../img/" + str + ".png"} alt=""></img>;
};

const TranslationsSummary = ({ notes }) => {
  return (
    <section>
      <div>{notes.split("").map((x) => printImages(x))}</div>
    </section>
  );
};

export default TranslationsSummary;
