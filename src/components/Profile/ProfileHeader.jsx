const ProfileHeader = ({ username }) => {
  return (
    <header>
      <h4>Hello, Welcome back {username}</h4>
    </header>
  );
};

export default ProfileHeader;
